<?php
ob_start();
if (strlen(session_id()) < 1) {
    session_start(); //Validamos si existe o no la sesión
}
require_once "../modelos/Usuario.php";

$usuario = new Usuario();

$idusuario = isset($_POST["idusuario"]) ? limpiarCadena($_POST["idusuario"]) : "";
$nombre    = isset($_POST["nombre"]) ? limpiarCadena($_POST["nombre"]) : "";
$apellidos        = isset($_POST["apellido"]) ? limpiarCadena($_POST["apellido"]) : "";
$fecha_nacimiento        = isset($_POST["fecha_naci"]) ? limpiarCadena($_POST["fecha_naci"]) : "";

$lugar_nacimiento = isset($_POST["lugar_nacimiento"]) ? limpiarCadena($_POST["lugar_nacimiento"]) : "";
$region_naci = isset($_POST["region_naci"]) ? limpiarCadena($_POST["region_naci"]) : "";
$provincia_naci = isset($_POST["provincia_naci"]) ? limpiarCadena($_POST["provincia_naci"]) : "";
$distrito_naci = isset($_POST["distrito_naci"]) ? limpiarCadena($_POST["distrito_naci"]) : "";
$lugar_domicilio  = isset($_POST["lugar_domicilio"]) ? limpiarCadena($_POST["lugar_domicilio"]) : "";
$region_dom  = isset($_POST["region_dom"]) ? limpiarCadena($_POST["region_dom"]) : "";
$provincia_dom  = isset($_POST["provincia_dom"]) ? limpiarCadena($_POST["provincia_dom"]) : "";
$distrito_dom  = isset($_POST["distrito_dom"]) ? limpiarCadena($_POST["distrito_dom"]) : "";

$carrera  = isset($_POST["carrera"]) ? limpiarCadena($_POST["carrera"]) : "";
$tiempo_estudio_inicio  = isset($_POST["tiempo_estudio_inicio"]) ? limpiarCadena($_POST["tiempo_estudio_inicio"]) : "";
$tiempo_estudio_fin  = isset($_POST["tiempo_estudio_fin"]) ? limpiarCadena($_POST["tiempo_estudio_fin"]) : "";
$periodo_academico  = isset($_POST["periodo_academico"]) ? limpiarCadena($_POST["periodo_academico"]) : "";
$numero_titulo  = isset($_POST["numero_titulo"]) ? limpiarCadena($_POST["numero_titulo"]) : "";
$nombre_titulo  = isset($_POST["nombre_titulo"]) ? limpiarCadena($_POST["nombre_titulo"]) : "";
$fecha_titulo  = isset($_POST["fecha_titulo"]) ? limpiarCadena($_POST["fecha_titulo"]) : "";
$periodo  = isset($_POST["periodo"]) ? limpiarCadena($_POST["periodo"]) : "";

$ie_grado  = isset($_POST["ie_grado"]) ? limpiarCadena($_POST["ie_grado"]) : "";
$ie_mencion  = isset($_POST["ie_mencion"]) ? limpiarCadena($_POST["ie_mencion"]) : "";
$ie_estudios  = isset($_POST["ie_estudios"]) ? limpiarCadena($_POST["ie_estudios"]) : "";
$ie_horas  = isset($_POST["ie_horas"]) ? limpiarCadena($_POST["ie_horas"]) : "";
$ie_ano_inicio  = isset($_POST["ie_ano_inicio"]) ? limpiarCadena($_POST["ie_ano_inicio"]) : "";
$ie_ano_fin  = isset($_POST["ie_ano_fin"]) ? limpiarCadena($_POST["ie_ano_fin"]) : "";
$ie_periodo  = isset($_POST["ie_periodo"]) ? limpiarCadena($_POST["ie_periodo"]) : "";

$il_institucion  = isset($_POST["il_institucion"]) ? limpiarCadena($_POST["il_institucion"]) : "";
$il_inicio  = isset($_POST["il_inicio"]) ? limpiarCadena($_POST["il_inicio"]) : "";
$il_termino  = isset($_POST["il_termino"]) ? limpiarCadena($_POST["il_termino"]) : "";
$il_cargo  = isset($_POST["il_cargo"]) ? limpiarCadena($_POST["il_cargo"]) : "";

$tipo_documento = isset($_POST["tipo_documento"]) ? limpiarCadena($_POST["tipo_documento"]) : "";
$num_documento  = isset($_POST["num_documento"]) ? limpiarCadena($_POST["num_documento"]) : "";
$direccion      = isset($_POST["direccion"]) ? limpiarCadena($_POST["direccion"]) : "";
$telefono       = isset($_POST["telefono"]) ? limpiarCadena($_POST["telefono"]) : "";
$email          = isset($_POST["email"]) ? limpiarCadena($_POST["email"]) : "";
$cargo          = isset($_POST["cargo"]) ? limpiarCadena($_POST["cargo"]) : "";
$login          = isset($_POST["login"]) ? limpiarCadena($_POST["login"]) : "";
$clave          = isset($_POST["clave"]) ? limpiarCadena($_POST["clave"]) : "";
$clave_anterior= isset($_POST["clave_anterior"]) ? limpiarCadena($_POST["clave_anterior"]) : "";
$imagen = isset($_POST["imagen"]) ? limpiarCadena($_POST["imagen"]) : "";
$pdf = isset($_POST["pdf"]) ? limpiarCadena($_POST["pdf"]) : "";

switch ($_GET["op"]) {
    case 'guardaryeditar':
        if (!isset($_SESSION["nombre"])) {
            header("Location: ../vistas/login.html"); //Validamos el acceso solo a los usuarios logueados al sistema.
        } else {
            //Validamos el acceso solo al usuario logueado y autorizado.
            if ($_SESSION['almacen'] == 1) {
                if (!file_exists($_FILES['imagen']['tmp_name']) || !is_uploaded_file($_FILES['imagen']['tmp_name'])) {
                    $imagen = $_POST["imagenactual"];
                } else {
                    $ext = explode(".", $_FILES["imagen"]["name"]);
                    if ($_FILES['imagen']['type'] == "image/jpg" || $_FILES['imagen']['type'] == "image/jpeg" || $_FILES['imagen']['type'] == "image/png") {
                        $imagen = round(microtime(true)) . '.' . end($ext);
                        move_uploaded_file($_FILES["imagen"]["tmp_name"], "../files/usuarios/" . $imagen);
                    }
                }
                //inicio para subir y renombrar pdf
                if (!file_exists($_FILES['pdf']['tmp_name']) || !is_uploaded_file($_FILES['pdf']['tmp_name'])) {
                    $pdf = $_POST["pdfactual"];
                } else {
                    $ext = explode(".", $_FILES["pdf"]["name"]);
                    if ($_FILES['pdf']['type'] == "application/pdf") {
                        $pdf = round(microtime(true)) . '.' . end($ext);
                        move_uploaded_file($_FILES["pdf"]["tmp_name"], "../files/usuarios/" . $pdf);
                    }
                }
                //fin para subir y renombrar pdf



                if (empty($idusuario)) {
                    //Hash SHA256 en la contraseña
                    $clavehash = hash("SHA256", $clave);

                    $rspta = $usuario->insertar($nombre, $apellidos, $fecha_nacimiento, $num_documento, $lugar_nacimiento,$region_naci,$provincia_naci,$distrito_naci, $lugar_domicilio,$region_dom,$provincia_dom,$distrito_dom, $carrera, $tiempo_estudio_inicio, $tiempo_estudio_fin, $periodo_academico, $numero_titulo, $nombre_titulo, $fecha_titulo, $periodo, $ie_grado, $ie_mencion, $ie_estudios, $ie_horas, $ie_ano_inicio, $ie_ano_fin, $ie_periodo, $il_institucion, $il_inicio, $il_termino, $il_cargo, $telefono, $email, $cargo, $login, $clavehash, $imagen, $pdf, $_POST['permiso']);
                    echo $rspta ? "Usuario registrado" : "No se pudieron registrar todos los datos del usuario, el usuario ya existe  INGRESE OTRO USUARIO";

                } else {
                    if ($clave==$clave_anterior){
                        $clavehash = $clave;
                    }else{
                        $clavehash = hash("SHA256", $clave);
                    }
                    
                    
                    $rspta = $usuario->editar($idusuario, $nombre, $apellidos, $fecha_nacimiento,$num_documento,$lugar_nacimiento,$region_naci,$provincia_naci,$distrito_naci, $lugar_domicilio,$region_dom,$provincia_dom,$distrito_dom, $carrera, $tiempo_estudio_inicio, $tiempo_estudio_fin, $periodo_academico, $numero_titulo, $nombre_titulo, $fecha_titulo, $periodo, $ie_grado, $ie_mencion, $ie_estudios, $ie_horas, $ie_ano_inicio, $ie_ano_fin, $ie_periodo, $il_institucion, $il_inicio, $il_termino, $il_cargo, $telefono, $email, $cargo, $login, $clavehash, $imagen, $pdf, $_POST['permiso']);
                    echo $rspta ? "Usuario actualizado" : "Usuario no se pudo actualizar";
                }
                //Fin de las validaciones de acceso
            } else {
                require 'noacceso.php';
            }
        }
        break;

    case 'desactivar':
        if (!isset($_SESSION["nombre"])) {
            header("Location: ../vistas/login.html"); //Validamos el acceso solo a los usuarios logueados al sistema.
        } else {
            //Validamos el acceso solo al usuario logueado y autorizado.
            if ($_SESSION['almacen'] == 1) {
                $rspta = $usuario->desactivar($idusuario);
                echo $rspta ? "Usuario Desactivado" : "Usuario no se puede desactivar";
                //Fin de las validaciones de acceso
            } else {
                require 'noacceso.php';
            }
        }
        break;

    case 'activar':
        if (!isset($_SESSION["nombre"])) {
            header("Location: ../vistas/login.html"); //Validamos el acceso solo a los usuarios logueados al sistema.
        } else {
            //Validamos el acceso solo al usuario logueado y autorizado.
            if ($_SESSION['almacen'] == 1) {
                $rspta = $usuario->activar($idusuario);
                echo $rspta ? "Usuario activado" : "Usuario no se puede activar";
                //Fin de las validaciones de acceso
            } else {
                require 'noacceso.php';
            }
        }
        break;
    case 'eliminar':
        if (!isset($_SESSION["nombre"])) {
            header("Location: ../vistas/login.html"); //Validamos el acceso solo a los usuarios logueados al sistema.
        } else {
            //Validamos el acceso solo al usuario logueado y autorizado.
            if ($_SESSION['almacen'] == 1) {
                $rspta = $usuario->eliminar($idusuario);
                echo $rspta ? "Usuario Eliminado" : "Usuario no se pudo eliminar";
                //Fin de las validaciones de acceso
            } else {
                require 'noacceso.php';
            }
        }
        break;
    case 'mostrar':
        if (!isset($_SESSION["nombre"])) {
            header("Location: ../vistas/login.html"); //Validamos el acceso solo a los usuarios logueados al sistema.
        } else {
            //Validamos el acceso solo al usuario logueado y autorizado.
            if ($_SESSION['almacen'] == 1) {
                $rspta = $usuario->mostrar($idusuario);
                //Codificar el resultado utilizando json
                echo json_encode($rspta);
                //Fin de las validaciones de acceso
            } else {
                require 'noacceso.php';
            }
        }
        break;

    case 'listar':
        if (!isset($_SESSION["nombre"])) {
            header("Location: ../vistas/login.html"); //Validamos el acceso solo a los usuarios logueados al sistema.
        } else {
            //Validamos el acceso solo al usuario logueado y autorizado.
            if ($_SESSION['almacen'] == 1) {

                // $_SESSION['idusuario'] iba como parametro pero ya no es necesario;
                $rspta = $usuario->listar();
                //Vamos a declarar un array
                $data = array();

                while ($reg = $rspta->fetch_object()) {

                    if ($_SESSION['idusuario'] == $reg->idusuario) {
                        $data[] = array(
                            "0" => ($reg->condicion) ? '<button class="btn btn-warning" onclick="mostrar(' . $reg->idusuario . ')"><i class="fa fa-pencil"></i></button> 
                            '
                                . ' <button class="btn btn-danger" onclick="desactivar(' . $reg->idusuario . ')"><i class="fa fa-close"></i></button> 
                            <button class="btn btn-warning" onclick="eliminar(' . $reg->idusuario . ')"><i class="fa fa-trash"></i></button>
                            ' : '<button class="btn btn-warning" onclick="mostrar(' . $reg->idusuario . ')"><i class="fa fa-pencil"></i></button>' .
                                ' <button class="btn btn-primary" onclick="activar(' . $reg->idusuario . ')"><i class="fa fa-check"></i></button> <button class="btn btn-warning" onclick="eliminar(' . $reg->idusuario . ')"><i class="fa fa-trash"></i></button>',
                            "1" => $reg->nombre,
                            "2" => $reg->apellidos,

                            // "3" => $reg->tipo_documento,
                            "3" => $reg->num_documento,

                            // "4" => $reg->fecha_nacimiento,
                            // "4" => ,
                            "4" => date("d-m-y", strtotime($reg->fecha_nacimiento)),
                            
                            // "5" => $reg->lugar_nacimiento,
                            // "6" => $reg->lugar_domicilio,
                            "5" => $reg->carrera,
                            // "8" => $reg->numero_titulo,
                            // "9" => $reg->nombre_titulo,
                            // "10" => $reg->periodo,
                            // "11" => $reg->ie_grado,
                            // "12" => $reg->ie_mencion,
                            // "13" => $reg->ie_estudios,
                            "6" => $reg->login,
                            // si la cadena es vacia o null devuelve false
                            "7" => !$reg->imagen == '' ? "<img src='../files/usuarios/" . $reg->imagen . "' height='50px' width='50px' >" : "<img src='../files/icono_tablas/sinimagen.png' height='50px' width='50px' >",
                            "8" => !$reg->pdf == '' ? "<a href='../files/usuarios/" . $reg->pdf . "' target=_><img src='../files/icono_tablas/pdf.svg' height='50px' width='50px' ></a>" : "<img src='../files/icono_tablas/nohaypdf.jpg' height='50px' width='50px' >",
                            "9" => ($reg->condicion) ? '<span class="label bg-green">Activado</span>' : '<span class="label bg-red">Desactivado</span>'
                        );
                        break;
                    } else {
                        continue;
                    }
                }
                while ($reg = $rspta->fetch_object()) {

                    // Aqui existe un problemon , ya que estamos trabajando con el id y deberiamos trabajar con el user,pero como puede variar esta seria una solucion rapida
                    if ($_SESSION['idusuario'] == 1) {

                        $data[] = array(
                            "0" => ($reg->condicion) ? '<button class="btn btn-warning" onclick="mostrar(' . $reg->idusuario . ')"><i class="fa fa-pencil"></i></button> 
                            '
                                . ' <button class="btn btn-danger" onclick="desactivar(' . $reg->idusuario . ')"><i class="fa fa-close"></i></button> 
                            <button class="btn btn-warning" onclick="eliminar(' . $reg->idusuario . ')"><i class="fa fa-trash"></i></button>
                            ' : '<button class="btn btn-warning" onclick="mostrar(' . $reg->idusuario . ')"><i class="fa fa-pencil"></i></button>' .
                                ' <button class="btn btn-primary" onclick="activar(' . $reg->idusuario . ')"><i class="fa fa-check"></i></button> <button class="btn btn-warning" onclick="eliminar(' . $reg->idusuario . ')"><i class="fa fa-trash"></i></button>',
                            "1" => $reg->nombre,
                            "2" => $reg->apellidos,

                            // "3" => $reg->tipo_documento,
                            "3" => $reg->num_documento,

                            // "4" => $reg->fecha_nacimiento,
                            // "4" => ,
                            "4" => date("d-m-y", strtotime($reg->fecha_nacimiento)),
                            
                            // "5" => $reg->lugar_nacimiento,
                            // "6" => $reg->lugar_domicilio,
                            "5" => $reg->carrera,
                            // "8" => $reg->numero_titulo,
                            // "9" => $reg->nombre_titulo,
                            // "10" => $reg->periodo,
                            // "11" => $reg->ie_grado,
                            // "12" => $reg->ie_mencion,
                            // "13" => $reg->ie_estudios,
                            "6" => $reg->login,
                            // si la cadena es vacia o null devuelve false
                            "7" => !$reg->imagen == '' ? "<img src='../files/usuarios/" . $reg->imagen . "' height='50px' width='50px' >" : "<img src='../files/icono_tablas/sinimagen.png' height='50px' width='50px' >",
                            "8" => !$reg->pdf == '' ? "<a href='../files/usuarios/" . $reg->pdf . "' target=_><img src='../files/icono_tablas/pdf.svg' height='50px' width='50px' ></a>" : "<img src='../files/icono_tablas/nohaypdf.jpg' height='50px' width='50px' >",
                            "9" => ($reg->condicion) ? '<span class="label bg-green">Activado</span>' : '<span class="label bg-red">Desactivado</span>'
                        );
                    } else {
                        break;
                    }
                }

                $results = array(
                    "sEcho"                => 1, //Información para el datatables
                    "iTotalRecords"        => count($data), //enviamos el total registros al datatable
                    "iTotalDisplayRecords" => count($data), //enviamos el total registros a visualizar
                    "aaData"               => $data
                );
                echo json_encode($results);
                //Fin de las validaciones de acceso
            } else {
                require 'noacceso.php';
            }
        }
        break;

    case 'permisos':
        //Obtenemos todos los permisos de la tabla permisos
        require_once "../modelos/Permiso.php";
        $permiso = new Permiso();
        $rspta   = $permiso->listar();

        //Obtener los permisos asignados al usuario
        $id       = $_GET['id'];
        $marcados = $usuario->listarmarcados($id);
        //Declaramos el array para almacenar todos los permisos marcados
        $valores = array();

        //Almacenar los permisos asignados al usuario en el array
        while ($per = $marcados->fetch_object()) {
            array_push($valores, $per->idpermiso);
        }

        //Mostramos la lista de permisos en la vista y si están o no marcados
        while ($reg = $rspta->fetch_object()) {
            // es obligatorio tener permiso 2 que es almacen(al parecer trabaja con alamcen)y 5 que es para acceso(crud egresados)
            $sw = in_array($reg->idpermiso, $valores) ? 'checked' : '';

            if ($reg->nombre == 'Acceso') {
                echo '<li style="visibility:hidden;display:none"> <input  type="checkbox" checked ' . $sw . '  name="permiso[]" value="' . $reg->idpermiso . '">' . $reg->nombre . '</li>';
            }
            // elseif  NO ES APROPIADO USARlo debido a que solo se ejecutara en una sola vez y nosotros queremos que se EJECUTE 2 VECES (acceso y almacen)
            // resulto todo lo contrario  SI NECESITO EL ELSEIF ,porque? POR EL ELSE SE EJECUTABA 2 VECES ACCESO
            elseif ($reg->nombre == 'Almacen') {
                echo '<li style="visibility:hidden;display:none "> <input  type="checkbox" checked ' . $sw . '  name="permiso[]" value="' . $reg->idpermiso . '">' . $reg->nombre . '</li>';
            } else {
                echo '<li> <input type="checkbox" ' . $sw . '  name="permiso[]" value="' . $reg->idpermiso . '">' . $reg->nombre . '</li>';
            }
        }
        break;

    case 'verificar':
        $logina = $_POST['logina'];
        $clavea = $_POST['clavea'];

        //Hash SHA256 en la contraseña
        $clavehash = hash("SHA256", $clavea);

        $rspta = $usuario->verificar($logina, $clavehash);

        $fetch = $rspta->fetch_object();

        if (isset($fetch)) {
            //Declaramos las variables de sesión
            $_SESSION['idusuario'] = $fetch->idusuario;
            $_SESSION['nombre']    = $fetch->nombre;
            $_SESSION['imagen']    = $fetch->imagen;
            $_SESSION['login']     = $fetch->login;

            //Obtenemos los permisos del usuario
            $marcados = $usuario->listarmarcados($fetch->idusuario);

            //Declaramos el array para almacenar todos los permisos marcados
            $valores = array();

            //Almacenamos los permisos marcados en el array
            while ($per = $marcados->fetch_object()) {
                array_push($valores, $per->idpermiso);
            }

            //Determinamos los accesos del usuario
            in_array(1, $valores) ? $_SESSION['escritorio'] = 1 : $_SESSION['escritorio'] = 0;
            in_array(2, $valores) ? $_SESSION['almacen']    = 1 : $_SESSION['almacen']    = 0;
            in_array(3, $valores) ? $_SESSION['compras']    = 1 : $_SESSION['compras']    = 0;
            in_array(4, $valores) ? $_SESSION['ventas']     = 1 : $_SESSION['ventas']     = 0;
            in_array(5, $valores) ? $_SESSION['acceso']     = 1 : $_SESSION['acceso']     = 0;
            in_array(6, $valores) ? $_SESSION['consultac']  = 1 : $_SESSION['consultac']  = 0;
            in_array(7, $valores) ? $_SESSION['consultav']  = 1 : $_SESSION['consultav']  = 0;
        }
        echo json_encode($fetch);
        break;

    case 'salir':
        //Limpiamos las variables de sesión
        session_unset();
        //Destruìmos la sesión
        session_destroy();
        //Redireccionamos al login
        header("Location: ../index.php");

        break;
}
ob_end_flush();
