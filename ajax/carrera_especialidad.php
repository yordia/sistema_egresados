<?php
ob_start();
if (strlen(session_id()) < 1) {
    session_start(); //Validamos si existe o no la sesión
}
if (!isset($_SESSION["nombre"])) {
    header("Location: ../vistas/login.html"); //Validamos el acceso solo a los usuarios logueados al sistema.
} else {
//Validamos el acceso solo al usuario logueado y autorizado.
    if ($_SESSION['almacen'] == 1) {
        require_once "../modelos/CarreraEspecialidad.php";

        $carrera = new CarreraEspecialidad();

        $idcategoria = isset($_POST["idcategoria"]) ? limpiarCadena($_POST["idcategoria"]) : "";
        $nombre      = isset($_POST["nombre"]) ? limpiarCadena($_POST["nombre"]) : "";
        $descripcion = isset($_POST["descripcion"]) ? limpiarCadena($_POST["descripcion"]) : "";

        switch ($_GET["op"]) {
            case 'guardaryeditar':
                if (empty($idcategoria)) {
                    $rspta = $carrera->insertar($nombre, $descripcion);
                    echo $rspta ? "Carrera registrada" : "Carrera no se pudo registrar";
                } else {
                    $rspta = $carrera->editar($idcategoria, $nombre, $descripcion);
                    echo $rspta ? "Carrera actualizada" : "Carrera no se pudo actualizar";
                }
                break;

            case 'desactivar':
                $rspta = $carrera->desactivar($idcategoria);
                echo $rspta ? "Categoría Desactivada" : "Categoría no se puede desactivar";
                break;

            case 'activar':
                $rspta = $carrera->activar($idcategoria);
                echo $rspta ? "Categoría activada" : "Categoría no se puede activar";
                break;

            case 'mostrar':
                $rspta = $carrera->mostrar($idcategoria);
                //Codificar el resultado utilizando json
                echo json_encode($rspta);
                break;

            case 'listar':
                $rspta = $carrera->listar();
                //Vamos a declarar un array
                $data = array();

                while ($reg = $rspta->fetch_object()) {
                    $data[] = array(
                        "0" => ($reg->condicion) ? '<button class="btn btn-warning" onclick="mostrar(' . $reg->idcarrera_espe . ')"><i class="fa fa-pencil"></i></button>' .
                        ' <button class="btn btn-danger" onclick="desactivar(' . $reg->idcarrera_espe . ')"><i class="fa fa-close"></i></button>' :
                        '<button class="btn btn-warning" onclick="mostrar(' . $reg->idcarrera_espe . ')"><i class="fa fa-pencil"></i></button>' .
                        ' <button class="btn btn-primary" onclick="activar(' . $reg->idcarrera_espe . ')"><i class="fa fa-check"></i></button>',
                        "1" => $reg->carrera_esp,
                        "2" => $reg->descripcion,
                        "3" => ($reg->condicion) ? '<span class="label bg-green">Activado</span>' :
                        '<span class="label bg-red">Desactivado</span>',
                    );
                }
                $results = array(
                    "sEcho"                => 1, //Información para el datatables
                    "iTotalRecords"        => count($data), //enviamos el total registros al datatable
                    "iTotalDisplayRecords" => count($data), //enviamos el total registros a visualizar
                    "aaData"               => $data);
                echo json_encode($results);

                break;
        }
//Fin de las validaciones de acceso
    } else {
        require 'noacceso.php';
    }
}
ob_end_flush();
