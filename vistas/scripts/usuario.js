var tabla;

//Función que se ejecuta al inicio
function init() {
	mostrarform(false);
	listar();
	// listar2();

	$("#formulario").on("submit", function (e) {
		guardaryeditar(e);
	})

	$("#imagenmuestra").hide();
	//Mostramos los permisos
	$.post("../ajax/usuario.php?op=permisos&id=", function (r) {
		$("#permisos").html(r);
	});
	$('#mAcceso').addClass("treeview active");
	$('#lUsuarios').addClass("active");
}

//Función limpiar
function limpiar() {
	$("#nombre").val("");
	$("#apellido").val("");
	$("#num_documento").val("");
	$("#fecha_naci").val("");
	
	$("#lugar_nacimiento").val("");
	$("#region_naci").val("");
	$("#provincia_naci").val("");
	$("#distrito_naci	").val("");

	$("#lugar_domicilio").val("");
	$("#region_dom").val("");
	$("#provincia_dom").val("");
	$("#distrito_dom").val("");

	$("#carrera").val("");
	$("#tiempo_estudio_inicio").val("");
	$("#tiempo_estudio_fin").val("");
	$("#periodo_academico").val("");

	$("#numero_titulo").val("");
	$("#nombre_titulo").val("");
	$("#fecha_titulo").val("");
	$("#periodo").val("");

	$("#ie_grado").val("");
	$("#pdf").val("");
	$("#ie_mencion").val("");
	$("#ie_estudios").val("");
	$("#ie_horas").val("");
	$("#ie_ano_inicio").val("");
	$("#ie_ano_fin").val("");
	$("#ie_periodo").val("");

	$("#il_institucion").val("");
	$("#il_inicio").val("");
	$("#il_termino").val("");
	$("#il_cargo").val("");


	$("#direccion").val("");
	$("#telefono").val("");
	$("#email").val("");
	$("#cargo").val("");
	$("#login").val("");
	$("#clave").val("");
	$("#imagenmuestra").attr("src", "");
	$("#imagenactual").val("");
	$("#idusuario").val("");
}

//Función mostrar formulario
function mostrarform(flag) {
	limpiar();
	if (flag) {
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled", false);
		$("#btnagregar").hide();
	}
	else {
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//Función cancelarform
function cancelarform() {
	limpiar();
	mostrarform(false);
}

//Función Listar
function listar() {
	tabla = $('#tbllistado').dataTable(
		{
			"lengthMenu": [10, 15, 25, 75, 100],//mostramos el menú de registros a revisar
			"aProcessing": true,//Activamos el procesamiento del datatables
			"aServerSide": true,//Paginación y filtrado realizados por el servidor
			dom: '<Bl<f>rtip>',//Definimos los elementos del control de tabla
			buttons: [
				// 'copyHtml5',
				'excelHtml5',
				// 'csvHtml5',
				// 'pdf'
			],
			"ajax":
			{
				url: '../ajax/usuario.php?op=listar',
				type: "get",
				dataType: "json",
				error: function (e) {
					console.log(e.responseText);
				}
			},
			"language": {
				"lengthMenu": "Mostrar : _MENU_ registros",
				"buttons": {
					"copyTitle": "Tabla Copiada",
					"copySuccess": {
						_: '%d líneas copiadas',
						1: '1 línea copiada'
					}
				}
			},
			"bDestroy": true,
			"iDisplayLength": 10,//Paginación
			"order": [[0, "desc"]]//Ordenar (columna,orden)
		}).DataTable();
}


//Función para guardar o editar

function guardaryeditar(e) {
	e.preventDefault(); //No se activará la acción predeterminada del evento
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/usuario.php?op=guardaryeditar",
		type: "POST",
		data: formData,
		contentType: false,
		processData: false,

		success: function (datos) {
			bootbox.alert(datos);
			mostrarform(false);
			tabla.ajax.reload();
		}

	});
	limpiar();
}

function mostrar(idusuario) {
	$.post("../ajax/usuario.php?op=mostrar", { idusuario: idusuario }, function (data, status) {
		data = JSON.parse(data);
		mostrarform(true);

		$("#nombre").val(data.nombre);
		$("#apellido").val(data.apellidos);

		diaActual = new Date(data.fecha_nacimiento);
		let day = diaActual.getDate();
		let month = diaActual.getMonth() + 1;
		let year = diaActual.getFullYear();
		let fecha = year + '-' + month + '-' + day;

		$("#fecha_naci").val(fecha);
		$("#num_documento").val(data.num_documento);
		$("#tipo_documento").selectpicker('refresh');

		$("#lugar_nacimiento").val(data.lugar_nacimiento);
		$("#region_naci").val(data.region_naci);
		$("#provincia_naci").val(data.provincia_naci);
		$("#distrito_naci	").val(data.distrito_naci);

		$("#lugar_domicilio").val(data.lugar_domicilio);
		$("#region_dom").val(data.region_dom);
		$("#provincia_dom").val(data.provincia_dom);
		$("#distrito_dom").val(data.distrito_dom);

		$("#carrera").val(data.carrera);

		tiempo_estudio_inicio = new Date(data.tiempo_estudio_inicio);
		day = tiempo_estudio_inicio.getDate();
		month = tiempo_estudio_inicio.getMonth() + 1;
		year = tiempo_estudio_inicio.getFullYear();
		fecha = year + '-' + month + '-' + day;
		$("#tiempo_estudio_inicio").val(fecha);

		tiempo_estudio_fin = new Date(data.tiempo_estudio_fin);
		day = tiempo_estudio_fin.getDate();
		month = tiempo_estudio_fin.getMonth() + 1;
		year = tiempo_estudio_fin.getFullYear();
		fecha = year + '-' + month + '-' + day;
		$("#tiempo_estudio_fin").val(fecha);

		$("#periodo_academico").val(data.periodo_academico);
		$("#numero_titulo").val(data.numero_titulo);
		$("#nombre_titulo").val(data.nombre_titulo);
		fecha_titulo = new Date(data.fecha_titulo);
		day = fecha_titulo.getDate();
		month = fecha_titulo.getMonth() + 1;
		year = fecha_titulo.getFullYear();
		fecha = year + '-' + month + '-' + day;
		$("#fecha_titulo").val(fecha);
		// $("#fecha_titulo").val(data.fecha_titulo);
		$("#periodo").val(data.periodo);
		$("#ie_grado").val(data.ie_grado);
		$("#ie_mencion").val(data.ie_mencion);
		$("#ie_estudios").val(data.ie_estudios);
		$("#ie_horas").val(data.ie_horas);
		ie_ano_inicio = new Date(data.ie_ano_inicio);
		day = ie_ano_inicio.getDate();
		month = ie_ano_inicio.getMonth() + 1;
		year = ie_ano_inicio.getFullYear();
		fecha = year + '-' + month + '-' + day;
		$("#ie_ano_inicio").val(fecha);

		ie_ano_fin = new Date(data.ie_ano_fin);
		day = ie_ano_fin.getDate();
		month = ie_ano_fin.getMonth() + 1;
		year = ie_ano_fin.getFullYear();
		fecha = year + '-' + month + '-' + day;
		$("#ie_ano_fin").val(fecha);

		$("#ie_periodo").val(data.ie_periodo);

		$("#il_institucion").val(data.il_institucion);

		il_inicio = new Date(data.il_inicio);
		day = il_inicio.getDate();
		month = il_inicio.getMonth() + 1;
		year = il_inicio.getFullYear();
		fecha = year + '-' + month + '-' + day;
		$("#il_inicio").val(fecha);
		// $("#il_inicio").val(data.il_inicio);

		il_termino = new Date(data.il_termino);
		day = il_termino.getDate();
		month = il_termino.getMonth() + 1;
		year = il_termino.getFullYear();
		fecha = year + '-' + month + '-' + day;
		$("#il_termino").val(fecha);
		// $("#il_termino").val(data.il_termino);
		$("#il_cargo").val(data.il_cargo);

		$("#direccion").val(data.direccion);
		$("#telefono").val(data.telefono);
		$("#email").val(data.email);
		$("#cargo").val(data.cargo);
		$("#login").val(data.login);
		$("#clave").val(data.clave);
		$("#clave_anterior").val(data.clave);
		// $("#clave").val('');
		$("#imagenmuestra").show();
		$("#imagenmuestra").attr("src", "../files/usuarios/" + data.imagen);
		$("#imagenactual").val(data.imagen);
		$("#pdfactual").val(data.pdf);
		$("#idusuario").val(data.idusuario);

	});
	$.post("../ajax/usuario.php?op=permisos&id=" + idusuario, function (r) {
		$("#permisos").html(r);
	});
}

//Función para desactivar registros
function desactivar(idusuario) {
	bootbox.confirm("¿Está Seguro de desactivar el usuario?", function (result) {
		if (result) {
			$.post("../ajax/usuario.php?op=desactivar", { idusuario: idusuario }, function (e) {
				bootbox.alert(e);
				tabla.ajax.reload();
			});
		}
	})
}

//Función para activar registros
function activar(idusuario) {
	bootbox.confirm("¿Está Seguro de activar el Usuario?", function (result) {
		if (result) {
			$.post("../ajax/usuario.php?op=activar", { idusuario: idusuario }, function (e) {
				bootbox.alert(e);
				tabla.ajax.reload();
			});
		}
	})
}

function eliminar(idusuario) {
	bootbox.confirm("¿Está Seguro de eliminar el Usuario?", function (result) {
		if (result) {
			$.post("../ajax/usuario.php?op=eliminar", { idusuario: idusuario }, function (e) {
				bootbox.alert(e);
				tabla.ajax.reload();
			});
		}
	})
}

init();