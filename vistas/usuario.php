<?php
//Activamos el almacenamiento en el buffer
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
  header("Location: login.html");
} else {
  require 'header.php';
  if ($_SESSION['acceso'] == 1) {
?>
    <!--Contenido-->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <?php
                if ($_SESSION['idusuario'] == 1) {
                  echo '<h1 class="box-title">Egresados <button class="btn btn-success" id="btnagregar" onclick="mostrarform(true)"><i class="fa fa-plus-circle"></i> Agregar</button> <a href="../reportes/rptusuarios.php" target="_blank"><button class="btn btn-info"><i class="fa fa-clipboard"></i> Reporte</button></a></h1>';
                } else {
                  echo '<h1 class="box-title">Egresados  <a href="../reportes/rptusuarios.php" target="_blank">
                <!--<button class="btn btn-info"><i class="fa fa-clipboard"></i> Reporte</button>-->
                </a></h1>';
                }
                ?>

                <div class="box-tools pull-right">
                </div>
              </div>
              <!-- /.box-header -->
              <!-- centro -->
              <div class="panel-body table-responsive" id="listadoregistros">
                <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
                  <thead>
                    <th>Opciones Editar/Desactivar/Borrar</th>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Dni</th>
                    <th>Nació</th>
                    <!-- <th>Lugar de Nacimiento</th>
                    <th>Lugar de Domicilio</th> -->
                    <th>Carrera</th>
                    <!-- <th>Numero Titulo</th>
                    <th>Nombre Titulo</th>
                    <th>Periodo</th>
                    <th>Grado</th>
                    <th>Mencion</th>
                    <th>Estudió</th> -->
                    <!-- <th>Email</th> -->
                    <th>Login</th>
                    <th>Foto</th>
                    <th>Pdf</th>
                    <th>Estado</th>
                  
                  </thead>
                  <tbody>

                  </tbody>
                  <tfoot>
                   
                  </tfoot>
                </table>
              </div>
              <div class="panel-body" id="formularioregistros">
                <form name="formulario" id="formulario" method="POST">
                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Nombres(*):</label>
                    <input type="hidden" name="idusuario" id="idusuario">
                    <input type="text" class="form-control" name="nombre" id="nombre" maxlength="100" placeholder="Nombre" required>
                  </div>

                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Apellidos(*):</label>
                    <!-- <input type="hidden" name="idapellido" id="idapellido"> -->
                    <input type="text" class="form-control" name="apellido" id="apellido" maxlength="100" placeholder="Apellido" required>
                  </div>



                  <!-- <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Tipo Documento(*):</label>
                            <select class="form-control select-picker" name="tipo_documento" id="tipo_documento" required>
                              <option value="DNI">DNI</option>
                              <option value="RUC">RUC</option>
                              <option value="CEDULA">CEDULA</option>
                            </select>
                          </div> -->

                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>DNI(*):</label>
                    <input type="number" class="form-control" name="num_documento" id="num_documento" maxlength="8" placeholder="Documento" required>
                  </div>

                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Fecha Nacimiento(*):</label>
                    <input type="date" class="form-control" name="fecha_naci" id="fecha_naci" required>
                  </div>

                  <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <label>Lugar de nacimiento:</label>
                    <input type="text" class="form-control" name="lugar_nacimiento" id="lugar_nacimiento" placeholder="Ingresa lugar nacimiento" maxlength="70">
                  </div>
                  <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <label>Region:</label>
                    <input type="text" class="form-control" name="region_naci" id="region_naci" placeholder="Ingrese el departamento" maxlength="70">
                  </div>
                  <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <label>Provincia:</label>
                    <input type="text" class="form-control" name="provincia_naci" id="provincia_naci" placeholder="Ingrese la provincia" maxlength="70">
                  </div>
                  <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <label>Distrito:</label>
                    <input type="text" class="form-control" name="distrito_naci" id="distrito_naci" placeholder="Ingrese el distrito" maxlength="70">
                  </div>

                  <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <label>Lugar de domicilio:</label>
                    <input type="text" class="form-control" name="lugar_domicilio" id="lugar_domicilio" placeholder="Ingresa lugar domicilio" maxlength="70">
                  </div>
                  <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <label>Region:</label>
                    <input type="text" class="form-control" name="region_dom" id="region_dom" placeholder="Ingrese departamento o región" maxlength="70">
                  </div>
                  <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <label>Provincia:</label>
                    <input type="text" class="form-control" name="provincia_dom" id="provincia_dom" placeholder="Ingrese la provincia" maxlength="70">
                  </div>
                  <div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <label>Distrito:</label>
                    <input type="text" class="form-control" name="distrito_dom" id="distrito_dom" placeholder="Ingrese el distrito" maxlength="70">
                  </div>


                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Carrera o Especialidad:</label>
                    <input type="text" class="form-control" name="carrera" id="carrera" maxlength="100" placeholder="Ingrese la carrera o especialidad">
                  </div>

                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Tiempo Estudios Año Inicio:</label>
                    <input type="date" class="form-control" name="tiempo_estudio_inicio" id="tiempo_estudio_inicio">
                  </div>
                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Tiempo Estudios Año Fin:</label>
                    <input type="date" class="form-control" name="tiempo_estudio_fin" id="tiempo_estudio_fin">
                  </div>

                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Periodo Académico:</label>
                    <input type="text" class="form-control" name="periodo_academico" id="periodo_academico" maxlength="50" placeholder="Periodo Académico">
                  </div>

                  <!-- <fieldset class=form-group> -->
                  <!-- <hr> -->

                  <legend style=''>
                    <h4>Inicio Registro Titulo</h4>
                  </legend>
                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <label>Número Título:</label>
                    <input type="text" class="form-control" name="numero_titulo" id="numero_titulo" maxlength="100" placeholder="Número Título">
                  </div>
                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <label>Nombre Título:</label>
                    <input type="text" class="form-control" name="nombre_titulo" id="nombre_titulo" maxlength="100" placeholder="Nombre Título">
                  </div>
                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <label>Fecha Emisión Título:</label>
                    <input type="date" class="form-control" name="fecha_titulo" id="fecha_titulo" maxlength="100" placeholder="Fecha Emision Título">
                  </div>
                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <label>Periodo:</label>
                    <input type="text" class="form-control" name="periodo" id="periodo" maxlength="100" placeholder="Periodo">
                  </div>

                  <legend>
                    <h4>Informacion Educativa</h4>
                  </legend>

                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <label>Grado:</label>
                    <input type="text" class="form-control" name="ie_grado" id="ie_grado" maxlength="100" placeholder="Ingrese el grado">
                  </div>


                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Adjuntar documento de grado en pdf:</label>
                    <input type="file" class="form-control" name="pdf" id="pdf" accept="application/pdf">
                    <input type="hidden" name="pdfactual" id="pdfactual">

                  </div>

                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <label>Mención:</label>
                    <input type="text" class="form-control" name="ie_mencion" id="ie_mencion" maxlength="100" placeholder="Mención">
                  </div>
                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <label>Institución de Estudios:</label>
                    <input type="text" class="form-control" name="ie_estudios" id="ie_estudios" maxlength="100" placeholder="Institución de Estudios">
                  </div>
                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <label>Horas:</label>
                    <input type="text" class="form-control" name="ie_horas" id="ie_horas" maxlength="100" placeholder="Horas">
                  </div>
                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <label>Año de Inicio:</label>
                    <input type="date" class="form-control" name="ie_ano_inicio" id="ie_ano_inicio" maxlength="100" placeholder="Año de Inicio">
                  </div>
                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <label>Año de fin:</label>
                    <input type="date" class="form-control" name="ie_ano_fin" id="ie_ano_fin" maxlength="100" placeholder="Año de fin">
                  </div>
                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <label>Periodo:</label>
                    <input type="text" class="form-control" name="ie_periodo" id="ie_periodo" maxlength="100" placeholder="Periodo">
                  </div>

                  <legend>
                    <h4>Información Laboral</h4>
                  </legend>
                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <label>Institucion donde laboro:</label>
                    <input type="text" class="form-control" name="il_institucion" id="il_institucion" maxlength="100" placeholder="Institucion donde laboro">
                  </div>
                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <label>Año Inicio:</label>
                    <input type="date" class="form-control" name="il_inicio" id="il_inicio" maxlength="100" placeholder="">
                  </div>
                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <label>Año de Termino:</label>
                    <input type="date" class="form-control" name="il_termino" id="il_termino" maxlength="100" placeholder="">
                  </div>
                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <label>Cargo:</label>
                    <input type="text" class="form-control" name="il_cargo" id="il_cargo" maxlength="100" placeholder="Cargo">
                  </div>
                  <legend>
                    <h4>Acceso de Egresados</h4>
                  </legend>

                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Usuario (*):</label>
                    <input type="text" class="form-control" name="login" id="login" maxlength="100" placeholder="Login" required>
                  </div>
                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Clave (*):</label>
                    <!-- hidden -->
                    <input type="hidden" class="form-control" name="clave_anterior" id="clave_anterior" maxlength="64" placeholder="Clave">
                    <input type="password" class="form-control" name="clave" id="clave" maxlength="64" placeholder="Clave" required>
                  </div>
                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Estudios:</label>
                    <ul style="list-style: none;" id="permisos">
                    </ul>
                  </div>

                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label>Imagen:</label>
                    <input type="file" class="form-control" name="imagen" id="imagen" accept="image/png, .jpeg, .jpg">
                    <input type="hidden" name="imagenactual" id="imagenactual">
                    <img src="" width="150px" height="100px" id="imagenmuestra">
                  </div>

                  <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>

                    <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                  </div>
                </form>
              </div>
              <!--Fin centro -->
            </div><!-- /.box -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
    <!--Fin-Contenido-->
  <?php
              } else {
                require 'noacceso.php';
              }
              require 'footer.php';
  ?>

  <script type="text/javascript" src="scripts/usuario.js"></script>
<?php
            }
            ob_end_flush();
?>