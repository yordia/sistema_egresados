<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";

Class Usuario
{
	//Implementamos nuestro constructor
	public function __construct()
	{

	}

	//Implementamos un método para insertar registros
	public function insertar($nombre,$apellidos,$fecha_naci_parametro,$num_documento,$lugar_nacimiento,$region_naci,$provincia_naci,$distrito_naci, $lugar_domicilio,$region_dom,$provincia_dom,$distrito_dom,$carrera,$tiempo_estudio_inicio,$tiempo_estudio_fin,$periodo_academico,$numero_titulo,$nombre_titulo,$fecha_titulo,$periodo,$ie_grado,$ie_mencion,$ie_estudios,$ie_horas,$ie_ano_inicio,$ie_ano_fin,$ie_periodo,$il_institucion,$il_inicio,$il_termino,$il_cargo, $telefono,$email,$cargo,$login,$clave,$imagen,$pdf,$permisos)
	{
		$sql="INSERT INTO usuario (nombre,apellidos,fecha_nacimiento,num_documento,lugar_nacimiento,region_naci,provincia_naci,distrito_naci,lugar_domicilio,region_dom,provincia_dom,distrito_dom,carrera,tiempo_estudio_inicio,tiempo_estudio_fin,periodo_academico,numero_titulo,nombre_titulo,fecha_titulo,periodo,ie_grado,ie_mencion,ie_estudios,ie_horas,ie_ano_inicio,ie_ano_fin,ie_periodo,il_institucion,il_inicio,il_termino,il_cargo,telefono,email,cargo,login,clave,imagen,pdf,condicion)
		VALUES ('$nombre','$apellidos','$fecha_naci_parametro','$num_documento','$lugar_nacimiento','$region_naci','$provincia_naci','$distrito_naci','$lugar_domicilio','$region_dom','$provincia_dom','$distrito_dom','$carrera','$tiempo_estudio_inicio','$tiempo_estudio_fin','$periodo_academico','$numero_titulo','$nombre_titulo','$fecha_titulo','$periodo','$ie_grado','$ie_mencion','$ie_estudios','$ie_horas','$ie_ano_inicio','$ie_ano_fin','$ie_periodo','$il_institucion','$il_inicio','$il_termino','$il_cargo','$telefono','$email','$cargo','$login','$clave','$imagen','$pdf','1')";
		//return ejecutarConsulta($sql);
		$idusuarionew=ejecutarConsulta_retornarID($sql);

		$num_elementos=0;
		$sw=true;

		while ($num_elementos < count($permisos))
		{
			$sql_detalle = "INSERT INTO usuario_permiso(idusuario, idpermiso) VALUES('$idusuarionew', '$permisos[$num_elementos]')";
			ejecutarConsulta($sql_detalle) or $sw = false;
			$num_elementos=$num_elementos + 1;
		}

		return $sw;
	}

	//Implementamos un método para editar registros
	public function editar($idusuario,$nombre,$apellidos,$fecha_naci_parametro,$num_documento,$lugar_nacimiento,$region_naci,$provincia_naci,$distrito_naci, $lugar_domicilio,$region_dom,$provincia_dom,$distrito_dom,$carrera,$tiempo_estudio_inicio,$tiempo_estudio_fin,$periodo_academico,$numero_titulo,$nombre_titulo,$fecha_titulo,$periodo,$ie_grado,$ie_mencion,$ie_estudios,$ie_horas,$ie_ano_inicio,$ie_ano_fin,$ie_periodo,$il_institucion,$il_inicio,$il_termino,$il_cargo, $telefono,$email,$cargo,$login,$clave,$imagen,$pdf,$permisos)
	{
		$sql="UPDATE usuario SET nombre='$nombre',apellidos='$apellidos',fecha_nacimiento='$fecha_naci_parametro',num_documento='$num_documento',lugar_nacimiento='$lugar_nacimiento',region_naci='$region_naci',provincia_naci='$provincia_naci',distrito_naci='$distrito_naci',lugar_domicilio='$lugar_domicilio',region_dom='$region_dom',provincia_dom='$provincia_dom',distrito_dom='$distrito_dom',carrera='$carrera',tiempo_estudio_inicio='$tiempo_estudio_inicio',tiempo_estudio_fin='$tiempo_estudio_fin',periodo_academico='$periodo_academico',numero_titulo='$numero_titulo',nombre_titulo='$nombre_titulo',fecha_titulo='$fecha_titulo',periodo='$periodo',ie_grado='$ie_grado',ie_mencion='$ie_mencion',ie_estudios='$ie_estudios',ie_horas='$ie_horas',ie_ano_inicio='$ie_ano_inicio',ie_ano_fin='$ie_ano_fin',ie_periodo='$ie_periodo',il_institucion='$il_institucion',il_inicio='$il_inicio',il_termino='$il_termino',il_cargo='$il_cargo',telefono='$telefono',email='$email',cargo='$cargo',login='$login',clave='$clave',imagen='$imagen',pdf='$pdf' WHERE idusuario='$idusuario'";
		ejecutarConsulta($sql);

		//Eliminamos todos los permisos asignados para volverlos a registrar
		$sqldel="DELETE FROM usuario_permiso WHERE idusuario='$idusuario'";
		ejecutarConsulta($sqldel);

		$num_elementos=0;
		$sw=true;

		while ($num_elementos < count($permisos))
		{
			$sql_detalle = "INSERT INTO usuario_permiso(idusuario, idpermiso) VALUES('$idusuario', '$permisos[$num_elementos]')";
			ejecutarConsulta($sql_detalle) or $sw = false;
			$num_elementos=$num_elementos + 1;
		}

		return $sw;

	}

	//Implementamos un método para desactivar categorías
	public function desactivar($idusuario)
	{
		$sql="UPDATE usuario SET condicion='0' WHERE idusuario='$idusuario'";
		return ejecutarConsulta($sql);
	}

	//Implementamos un método para activar categorías
	public function activar($idusuario)
	{
		$sql="UPDATE usuario SET condicion='1' WHERE idusuario='$idusuario'";
		return ejecutarConsulta($sql);
	}

	//Implementar un método para mostrar los datos de un registro a modificar
	public function mostrar($idusuario)
	{
		$sql="SELECT * FROM usuario WHERE idusuario='$idusuario'";
		return ejecutarConsultaSimpleFila($sql);
	}

	//Implementar un método para listar los registros
	public function listar($idegresado=34)
	{
		// si el que ha iniciado devuelve idusuario=1  entonces listar todo(return igual)
		// si no listar solo el id por parametro

		// $sql="SELECT * FROM usuario WHERE idusuario='$idegresado' ";
		$sql="SELECT * FROM usuario where condicion=1 or condicion=0  ";
	
		return ejecutarConsulta($sql);		
	}
	// para listar solo informacion del egresado
	public function listar2($idegresado=33)
	{
		$sql="SELECT * FROM usuario WHERE idusuario='$idegresado' ";
		return ejecutarConsulta($sql);		
	}
	//Implementar un método para listar los permisos marcados
	public function listarmarcados($idusuario)
	{
		$sql="SELECT * FROM usuario_permiso WHERE idusuario='$idusuario'";
		return ejecutarConsulta($sql);
	}

	//Función para verificar el acceso al sistema
	public function verificar($login,$clave)
    {
    	$sql="SELECT idusuario,nombre,num_documento,email,cargo,imagen,login FROM usuario WHERE login='$login' AND clave='$clave' AND condicion='1'"; 
    	return ejecutarConsulta($sql);  
	}
	
	public function eliminar($idusuario)
	{
		$sql="UPDATE usuario SET condicion='3' WHERE idusuario='$idusuario'";
		return ejecutarConsulta($sql);
	}

}

?>