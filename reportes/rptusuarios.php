<?php
//Activamos el almacenamiento en el buffer
ob_start();
if (strlen(session_id()) < 1) 
  session_start();

if (!isset($_SESSION["nombre"]))
{
  echo 'Debe ingresar al sistema correctamente para visualizar el reporte';
}
else
{
if ($_SESSION['acceso']==1)
{

//Inlcuímos a la clase PDF_MC_Table
require('PDF_MC_Table.php');
 
//Instanciamos la clase para generar el documento pdf
// agregamos al objeto  el parametro l para que sea horizontal
$pdf=new PDF_MC_Table('L');
 
//Agregamos la primera página al documento pdf
$pdf->AddPage();
 
//Seteamos el inicio del margen superior en 25 pixeles 
$y_axis_initial = 25;
 
//Seteamos el tipo de letra y creamos el título de la página. No es un encabezado no se repetirá
$pdf->SetFont('Arial','B',12);

$pdf->Cell(40,6,'',0,0,'C');
$pdf->Cell(195,6,'LISTA DE EGRESADOS',1,0,'C'); 
$pdf->Ln(10);
 
//Creamos las celdas para los títulos de cada columna y le asignamos un fondo gris y el tipo de letra
$pdf->SetFillColor(232,232,232); 
$pdf->SetFont('Arial','B',10);
$pdf->Cell(40,6,'Nombre',1,0,'C',1); 
$pdf->Cell(40,6,'Apellidos',1,0,'C',1); 
$pdf->Cell(20,6,'Nacio',1,0,'C',1);
$pdf->Cell(18,6,utf8_decode('Dni'),1,0,'C',1);
$pdf->Cell(25,6,utf8_decode('Carrera'),1,0,'C',1);
$pdf->Cell(25,6,utf8_decode('Numero Titulo'),1,0,'C',1);
$pdf->Cell(25,6,utf8_decode('Nombre Titulo'),1,0,'C',1);
$pdf->Cell(25,6,utf8_decode('Grado'),1,0,'C',1);
$pdf->Cell(35,6,utf8_decode('Estudió'),1,0,'C',1);
// $pdf->Cell(46,6,'Email',1,0,'C',1);
$pdf->Cell(25,6,utf8_decode('Usuario'),1,0,'C',1);
 
$pdf->Ln(10);
//Comenzamos a crear las filas de los registros según la consulta mysql
require_once "../modelos/Usuario.php";
$usuario = new Usuario();

// listar con parametro de inicio sesion,pero el admin,JODERR,SOLUCIONES?
$rspta = $usuario->listar();

//Table with rows and columns
$pdf->SetWidths(array(40,40,20,18,25,25,25,25,35,25));

while($reg= $rspta->fetch_object())
{  
    $nombre = $reg->nombre;
    $apellidox = $reg->apellidos;
    $fecha_nacimiento = date("d-m-y", strtotime($reg->fecha_nacimiento));
    // $tipo_documento = $reg->tipo_documento;
    $num_documento = $reg->num_documento;
    $carrera = $reg->carrera;
    $numero_titulo = $reg->numero_titulo;
    $nombre_titulo = $reg->nombre_titulo;
    $ie_grado = $reg->ie_grado;
    $ie_estudios = $reg->ie_estudios;
    // $email =$reg->email;
    $login =$reg->login;
 	
 	$pdf->SetFont('Arial','',10);
    $pdf->Row(array(utf8_decode($nombre),utf8_decode($apellidox),$fecha_nacimiento,$num_documento,utf8_decode($carrera),$numero_titulo,utf8_decode($nombre_titulo),$ie_grado,utf8_decode($ie_estudios),utf8_decode($login)));
}
 
//Mostramos el documento pdf
$pdf->Output();

?>
<?php
}
else
{
  echo 'No tiene permiso para visualizar el reporte';
}

}
ob_end_flush();
?>