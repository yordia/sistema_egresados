-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-12-2019 a las 13:48:26
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbsistema_egresado_copia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrera_especialidad`
--

CREATE TABLE `carrera_especialidad` (
  `idcarrera_espe` int(11) NOT NULL,
  `carrera_esp` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `condicion` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `carrera_especialidad`
--

INSERT INTO `carrera_especialidad` (`idcarrera_espe`, `carrera_esp`, `descripcion`, `condicion`) VALUES
(2, 'carrera edit', 'especi', 1),
(3, 'carr', 'ese', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso`
--

CREATE TABLE `permiso` (
  `idpermiso` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permiso`
--

INSERT INTO `permiso` (`idpermiso`, `nombre`) VALUES
(1, 'Estudios Técnicos'),
(2, 'Almacen'),
(3, 'Estudios Universitarios'),
(5, 'Acceso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellidos` varchar(150) DEFAULT NULL,
  `fecha_nacimiento` datetime DEFAULT NULL,
  `num_documento` varchar(20) DEFAULT NULL,
  `lugar_nacimiento` varchar(70) DEFAULT NULL,
  `region_naci` varchar(50) DEFAULT NULL,
  `provincia_naci` varchar(50) DEFAULT NULL,
  `distrito_naci` varchar(50) DEFAULT NULL,
  `lugar_domicilio` varchar(150) DEFAULT NULL,
  `region_dom` varchar(50) DEFAULT NULL,
  `provincia_dom` varchar(50) DEFAULT NULL,
  `distrito_dom` varchar(50) DEFAULT NULL,
  `carrera` varchar(100) DEFAULT NULL,
  `tiempo_estudio_inicio` datetime DEFAULT NULL,
  `tiempo_estudio_fin` datetime DEFAULT NULL,
  `periodo_academico` varchar(50) DEFAULT NULL,
  `numero_titulo` varchar(30) DEFAULT NULL,
  `nombre_titulo` varchar(100) DEFAULT NULL,
  `fecha_titulo` datetime DEFAULT NULL,
  `periodo` varchar(30) DEFAULT NULL,
  `ie_grado` varchar(30) DEFAULT NULL,
  `ie_mencion` varchar(100) DEFAULT NULL,
  `ie_estudios` varchar(100) DEFAULT NULL,
  `ie_horas` varchar(10) DEFAULT NULL,
  `ie_ano_inicio` datetime DEFAULT NULL,
  `ie_ano_fin` datetime DEFAULT NULL,
  `ie_periodo` varchar(30) DEFAULT NULL,
  `il_institucion` varchar(100) DEFAULT NULL,
  `il_inicio` datetime DEFAULT NULL,
  `il_termino` datetime DEFAULT NULL,
  `il_cargo` varchar(100) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `cargo` varchar(20) DEFAULT NULL,
  `login` varchar(20) NOT NULL,
  `clave` varchar(64) NOT NULL,
  `imagen` varchar(50) NOT NULL DEFAULT '1576604120.png',
  `pdf` varchar(200) DEFAULT NULL,
  `condicion` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nombre`, `apellidos`, `fecha_nacimiento`, `num_documento`, `lugar_nacimiento`, `region_naci`, `provincia_naci`, `distrito_naci`, `lugar_domicilio`, `region_dom`, `provincia_dom`, `distrito_dom`, `carrera`, `tiempo_estudio_inicio`, `tiempo_estudio_fin`, `periodo_academico`, `numero_titulo`, `nombre_titulo`, `fecha_titulo`, `periodo`, `ie_grado`, `ie_mencion`, `ie_estudios`, `ie_horas`, `ie_ano_inicio`, `ie_ano_fin`, `ie_periodo`, `il_institucion`, `il_inicio`, `il_termino`, `il_cargo`, `telefono`, `email`, `cargo`, `login`, `clave`, `imagen`, `pdf`, `condicion`) VALUES
(1, 'Juan', 'ñoño', '2019-12-15 00:00:00', '47715777', 'naci', 'region nacio admin', NULL, NULL, 'edit', NULL, NULL, NULL, 'carrerax', '2019-12-20 00:00:00', '2019-12-21 00:00:00', 'periodo admin', 'numero titu admin', 'nmbre titulo admin ñoño', '2019-12-22 00:00:00', 'peridoo', 'ie grado', 'ie mencion', 'San Luis Gonzaga de ica', 'ie horas', '2019-12-23 00:00:00', '2019-12-24 00:00:00', 'ie perido', 'il institucion', '2019-12-25 00:00:00', '2019-12-26 00:00:00', 'il cargo', '', '', '', 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1576604120.png', '1576604120.pdf', 1),
(33, 'Juan 2 edit', 'ñoño', '2019-12-15 00:00:00', '47715777', 'naci', 'region edit', 'proovi edit', 'distrito', 'edit', '', '', '', 'carrerax', '2019-12-20 00:00:00', '2019-12-21 00:00:00', 'periodo admin', 'numero titu admin', 'nmbre titulo admin ñoño', '2019-12-22 00:00:00', 'peridoo', 'ie grado', 'ie mencion', 'San Luis Gonzaga de ica', 'ie horas', '2019-12-23 00:00:00', '2019-12-24 00:00:00', 'ie perido', 'il institucion', '2019-12-25 00:00:00', '2019-12-26 00:00:00', 'il cargo', '', '', '', 'admin2', '1C142B2D01AA34E9A36BDE480645A57FD69E14155DACFAB5A3F9257B77FDC8D8', '1576604120.png', '1576604120.pdf', 0),
(34, 'Juan 3', 'ñoño', '2019-12-15 00:00:00', '47715777', 'naci', NULL, NULL, NULL, 'edit', NULL, NULL, NULL, 'carrerax', '2019-12-20 00:00:00', '2019-12-21 00:00:00', 'periodo admin', 'numero titu admin', 'nmbre titulo admin ñoño', '2019-12-22 00:00:00', 'peridoo', 'ie grado', 'ie mencion', 'San Luis Gonzaga de ica', 'ie horas', '2019-12-23 00:00:00', '2019-12-24 00:00:00', 'ie perido', 'il institucion', '2019-12-25 00:00:00', '2019-12-26 00:00:00', 'il cargo', '', '', '', 'admin3', '4FC2B5673A201AD9B1FC03DCB346E1BAAD44351DAA0503D5534B4DFDCC4332E0', '1576604120.png', '1576604120.pdf', 1),
(35, 'juan 4', 'ape', '2019-12-18 00:00:00', '70074321', 'lugar naci', NULL, NULL, NULL, 'lugar domi', NULL, NULL, NULL, 'carrera', '2019-12-19 00:00:00', '2019-12-20 00:00:00', 'periodo academico', 'numerro titulo', 'nombre tirtulo', '2019-12-21 00:00:00', 'perioso solo', 'grado', 'mencn', 'i estudios', '4', '2019-12-22 00:00:00', '2019-12-23 00:00:00', 'periodo solo 2', 'i donde laboro', '2019-12-23 00:00:00', '2019-12-24 00:00:00', 'cargito', '', '', '', 'admin4', '110198831a426807bccd9dbdf54b6dcb5298bc5d31ac49069e0ba3d210d970ae', '1576604120.png', '1576720371.pdf', 1),
(39, 'miau2', 'miau2', '0000-00-00 00:00:00', '222', '', NULL, NULL, NULL, '', NULL, NULL, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', '', 'miau2', '1fe1bca57901e970308bc63c095c29e78abd241c3c5c21154977d0b26a6908b4', '', '', 3),
(40, 'miau3', 'miau3', '2019-12-18 00:00:00', '3333', '', NULL, NULL, NULL, '', NULL, NULL, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', '', 'miau3', '739a0984d2c0c5eaa2b85491491c277fb0ee50f012b81b4055ab84269b59d1c0', '', '', 3),
(41, 'no tiene permiso x q lo llene desde base datos', 'miau4', '2019-12-18 00:00:00', '3333', '', NULL, NULL, NULL, '', NULL, NULL, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', '', 'miau4', '739a0984d2c0c5eaa2b85491491c277fb0ee50f012b81b4055ab84269b59d1c0', '', '', 3),
(42, 'miau5', 'miau5', '2019-12-18 00:00:00', '3333', '', NULL, NULL, NULL, '', NULL, NULL, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', '', 'miau5', '739a0984d2c0c5eaa2b85491491c277fb0ee50f012b81b4055ab84269b59d1c0', '', '', 3),
(43, 'hola', 'hola', '2019-12-19 00:00:00', '123456', 'naci', 'region', 'provin', 'distrito', 'domic', 'region domi', 'provinci a dom', 'distrito dom', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', '', '', 'hola', 'b221d9dbb083a7f33428d7c2a3c3198ae925614d70210e28716ccaa7cd4ddb79', '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_permiso`
--

CREATE TABLE `usuario_permiso` (
  `idusuario_permiso` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `idpermiso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_permiso`
--

INSERT INTO `usuario_permiso` (`idusuario_permiso`, `idusuario`, `idpermiso`) VALUES
(275, 1, 1),
(276, 1, 2),
(277, 1, 3),
(278, 1, 4),
(279, 1, 5),
(285, 34, 1),
(286, 34, 2),
(287, 34, 3),
(288, 34, 4),
(289, 34, 5),
(293, 35, 2),
(294, 35, 5),
(295, 36, 2),
(296, 36, 5),
(301, 39, 2),
(302, 39, 5),
(303, 40, 2),
(304, 40, 5),
(309, 43, 2),
(310, 43, 5),
(311, 33, 1),
(312, 33, 2),
(313, 33, 3),
(314, 33, 5);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `carrera_especialidad`
--
ALTER TABLE `carrera_especialidad`
  ADD PRIMARY KEY (`idcarrera_espe`);

--
-- Indices de la tabla `permiso`
--
ALTER TABLE `permiso`
  ADD PRIMARY KEY (`idpermiso`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`),
  ADD UNIQUE KEY `login_UNIQUE` (`login`);

--
-- Indices de la tabla `usuario_permiso`
--
ALTER TABLE `usuario_permiso`
  ADD PRIMARY KEY (`idusuario_permiso`),
  ADD KEY `fk_usuario_permiso_permiso_idx` (`idpermiso`),
  ADD KEY `fk_usuario_permiso_usuario_idx` (`idusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `carrera_especialidad`
--
ALTER TABLE `carrera_especialidad`
  MODIFY `idcarrera_espe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `permiso`
--
ALTER TABLE `permiso`
  MODIFY `idpermiso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT de la tabla `usuario_permiso`
--
ALTER TABLE `usuario_permiso`
  MODIFY `idusuario_permiso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=315;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `usuario_permiso`
--
ALTER TABLE `usuario_permiso`
  ADD CONSTRAINT `fk_usuario_permiso_permiso` FOREIGN KEY (`idpermiso`) REFERENCES `permiso` (`idpermiso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuario_permiso_usuario` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
