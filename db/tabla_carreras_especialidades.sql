CREATE TABLE `carrera_especialidad` (
  `idcarrera_espe` int(11) NOT NULL,
  `carrera` varchar(100) NOT NULL,
  `especialidad` varchar(100) DEFAULT NULL,
  `condicion` tinyint(1) NOT NULL DEFAULT '1'
)


alter table carrera_especialidad add primary key (idcarrera_espe) 

ALTER TABLE `carrera_especialidad` MODIFY `idcarrera_espe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
  